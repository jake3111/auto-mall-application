package com.example.demo.service;

import com.example.demo.domain.Car;
import com.example.demo.repository.CarRepository;
import org.springframework.stereotype.Service;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class CarService {

    private CarRepository carRepository;

    public CarService(CarRepository carRepository) {
        this.carRepository = carRepository;
    }

    public Car saveCar(Car car){
        return carRepository.save(car);
    }

    public List<Car> getCars(String color, String type) {
        if (color != null && type != null){
            return StreamSupport.stream(carRepository.findAll().spliterator(), false)
                    .filter(c -> c.getColor().equals(color) && c.getType().toString().equals(type))
                    .collect(Collectors.toList());
        } else if (color != null){
            return StreamSupport.stream(carRepository.findAll().spliterator(), false)
                    .filter(c -> c.getColor().equals(color))
                    .collect(Collectors.toList());
        } else if (type != null){
            return StreamSupport.stream(carRepository.findAll().spliterator(), false)
                    .filter(c -> c.getType().toString().equals(type))
                    .collect(Collectors.toList());
        } else {
            return StreamSupport.stream(carRepository.findAll().spliterator(), false)
                    .collect(Collectors.toList());
        }
    }

    public Car getCarById(long id){
        return carRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("User not found for this id :: " + id));
    }

    public void deleteCarById(long id){
        if (carRepository.existsById(id)){
            carRepository.deleteById(id);
        } else {
            throw new ResourceNotFoundException("Could not find a card with this id to delete: " + id);
        }
    }
}
