package com.example.demo.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.apache.commons.lang3.StringUtils;
import javax.persistence.Id;

import javax.persistence.*;

@Entity
@Table(name = "car")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Car {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String year;
    private String make;
    private String model;
    private CarType type;
    private String color;
    private String owner;

    public Car(){

    }

    public Car(String year, String make, String model, CarType type, String color, String owner) {
        this.year = year;
        this.make = make;
        this.model = model;
        this.type = type;
        this.color = color;
        this.owner = owner;
    }

    public Car(long id, String year, String make, String model, CarType type, String color, String owner) {
        this.id = id;
        this.year = year;
        this.make = make;
        this.model = model;
        this.type = type;
        this.color = color;
        this.owner = owner;
    }

    public Car(long id, String year, String make, String model, CarType type, String color) {
        this.id = id;
        this.year = year;
        this.make = make;
        this.model = model;
        this.type = type;
        this.color = color;
    }

    public Long getId() {
        return id;
    }

    public String getYear() {
        return year;
    }

    public String getMake() {
        return make;
    }

    public String getModel() {
        return model;
    }

    public CarType getType() {
        return type;
    }

    public String getColor() {
        return color;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Car))
        {
            return false;
        }
        Car comparisionCar = (Car)obj;
        if (this.type == comparisionCar.type
                        && StringUtils.equals(this.color, comparisionCar.color)
                        && this.id == comparisionCar.id
                        && StringUtils.equals(this.make, comparisionCar.make)
                        && StringUtils.equals(this.model, comparisionCar.model)
                        && StringUtils.equals(this.year, comparisionCar.year)) {
            return true;
        }
        return false;
    }
}
