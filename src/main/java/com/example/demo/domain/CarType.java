package com.example.demo.domain;

public enum CarType {
    SEDAN, COUPE, CONVERTIBLE, SUV, HYBRID, CROSSOVER
}
