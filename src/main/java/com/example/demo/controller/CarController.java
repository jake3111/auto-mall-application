package com.example.demo.controller;

import com.example.demo.domain.Car;
import com.example.demo.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/cars")
public class CarController {

    private CarService carService;

    @Autowired
    public CarController(CarService carService){
        this.carService = carService;
    }

    @GetMapping("/{id}")
    public Car getCarById(@PathVariable Long id) {
        return this.carService.getCarById(id);
    }

    @PostMapping("")
    public Car create(@RequestBody Car postCar) {
        return this.carService.saveCar(postCar);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteCar(@PathVariable Long id) {
        this.carService.deleteCarById(id);

        return ResponseEntity.ok().build();
    }

    @GetMapping("")
    public List<Car> getCarsWithFilters(@RequestParam Map<String, String> requestParams) {
        return this.carService.getCars(requestParams.get("c"), requestParams.get("t"));
    }

    @PatchMapping("/{id}")
    public Car updateCarOwner(@PathVariable(value = "id") Long carId, @RequestBody Map<String, String> requestBody) {
        Car currentCar = this.carService.getCarById(carId);
        currentCar.setOwner(requestBody.get("owner"));
        return this.carService.saveCar(currentCar);
    }
}
