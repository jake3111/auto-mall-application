package com.example.demo.service;

import com.example.demo.domain.Car;
import com.example.demo.domain.CarType;
import com.example.demo.repository.CarRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;

import java.util.Optional;
import java.util.LinkedList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class CarServiceTests {

    @Mock
    private CarRepository carRepository;
    private CarService carService;
    private Car testCar;


    @BeforeEach
    void setUp() {
        //setting up car
        long id = 1L;
        String year = "2012";
        String make = "Ford";
        String model = "Mustang";
        CarType type = CarType.HYBRID;
        String color = "Red";
        this.testCar = new Car(id, year, make, model, type, color);

        this.carService = new CarService(carRepository);
    }

    @Test
    public void addNewCar(){
        //setting up car
        long id = 1L;
        String year = "2012";
        String make = "Ford";
        String model = "Mustang";
        CarType type = CarType.HYBRID;
        String color = "Red";
        Car postCar = new Car(id, year, make, model, type, color);
        when(carRepository.save(any(Car.class))).thenReturn(postCar);
        assertEquals(postCar, carService.saveCar(postCar));
    }

    @Test
    public void getCarsWithFilters() {
        List<Car> carList = new LinkedList<>();
        carList.add(this.testCar);
        long id = 2L;
        String year = "2016";
        String make = "Nissan";
        String model = "Pickup";
        CarType type = CarType.SEDAN;
        String color = "Blue";
        Car car2 = new Car(id, year, make, model, type, color);
        carList.add(car2);

        when(carRepository.findAll()).thenReturn(carList);

        List<Car> expected = this.carService.getCars("Blue", CarType.SEDAN.toString());

        assertEquals(1, expected.size());
        assertEquals(2L, expected.get(0).getId());
        assertEquals("Nissan", expected.get(0).getMake());
    }

    @Test
    public void getCarsTypeNoColor() {
        List<Car> carList = new LinkedList<>();
        carList.add(this.testCar);
        long id = 2L;
        String year = "2016";
        String make = "Nissan";
        String model = "Pickup";
        CarType type = CarType.SEDAN;
        String color = "Blue";
        Car car2 = new Car(id, year, make, model, type, color);
        carList.add(car2);

        when(carRepository.findAll()).thenReturn(carList);

        List<Car> expected = this.carService.getCars(null, CarType.SEDAN.toString());

        assertEquals(1, expected.size());
        assertEquals(2L, expected.get(0).getId());
        assertEquals("Nissan", expected.get(0).getMake());
    }

    @Test
    public void getCarsNoTypeColor() {
        List<Car> carList = new LinkedList<>();
        carList.add(this.testCar);
        long id = 2L;
        String year = "2016";
        String make = "Nissan";
        String model = "Pickup";
        CarType type = CarType.SEDAN;
        String color = "Blue";
        Car car2 = new Car(id, year, make, model, type, color);
        carList.add(car2);

        when(carRepository.findAll()).thenReturn(carList);

        List<Car> expected = this.carService.getCars("Blue", null);

        assertEquals(1, expected.size());
        assertEquals(2L, expected.get(0).getId());
        assertEquals("Nissan", expected.get(0).getMake());
    }

    @Test
    public void getAllCars() {
        List<Car> carList = new LinkedList<>();
        carList.add(this.testCar);
        long id = 2L;
        String year = "2016";
        String make = "Nissan";
        String model = "Pickup";
        CarType type = CarType.SEDAN;
        String color = "Blue";
        Car car2 = new Car(id, year, make, model, type, color);
        carList.add(car2);

        when(carRepository.findAll()).thenReturn(carList);

        List<Car> expected = this.carService.getCars(null, null);

        assertEquals(2, expected.size());
        assertEquals(2L, expected.get(1).getId());
        assertEquals("Nissan", expected.get(1).getMake());
        assertEquals(1L, expected.get(0).getId());
        assertEquals("Ford", expected.get(0).getMake());
    }

    @Test
    public void updateCar() {
        when(carRepository.save(any(Car.class))).thenReturn(this.testCar);

        this.testCar.setOwner("New Owner");
        Car updatedCar = this.carService.saveCar(this.testCar);

        assertEquals(updatedCar.getOwner(), this.testCar.getOwner());
    }

    @Test
    public void getCarById(){
        long id = 1L;
        String year = "2012";
        String make = "Ford";
        String model = "Mustang";
        CarType type = CarType.HYBRID;
        String color = "Red";
        Optional<Car> returnCar = Optional.of(new Car(id, year, make, model, type, color));
        when(carRepository.findById(id)).thenReturn(returnCar);
        assertEquals(returnCar.get(), carService.getCarById(1L));
    }

    @Test
    public void deleteCar(){
        long id = 1L;
        when(carRepository.existsById(id)).thenReturn(true);
        carService.deleteCarById(1L);
    }

    @Test
    public void deleteCarExpectResourceNotFoundException(){
        long id = 1L;
        when(carRepository.existsById(id)).thenReturn(false);
        try{
            carService.deleteCarById(1L);
            fail();
        } catch (ResourceNotFoundException e){
        }
    }
}
