package com.example.demo.controller;

import com.example.demo.domain.Car;
import com.example.demo.domain.CarType;
import com.example.demo.service.CarService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeAll;
import com.google.gson.Gson;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.util.NestedServletException;

import java.io.File;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

@WebMvcTest(CarController.class)
@AutoConfigureMockMvc
public class CarControllerTests {

    @Autowired
    MockMvc mvc;

    @MockBean
    private CarService carService;

    private Car testCar;
    private static ObjectMapper objectMapper;

    @BeforeAll
    static void beforeAll() {
        objectMapper = new ObjectMapper();
    }

    @BeforeEach
    void setUp() {
        //setting up car
        long id = 1L;
        String year = "2012";
        String make = "Ford";
        String model = "Mustang";
        CarType type = CarType.HYBRID;
        String color = "Red";
        this.testCar = new Car(id, year, make, model, type, color);
    }

  
    @Test
    public void getCarsWithFilters() throws Exception{
        //creating a new unique car
        long id = 2L;
        String year = "2016";
        String make = "Nissan";
        String model = "Pickup";
        CarType type = CarType.SEDAN;
        String color = "Blue";
        String owner = "Fred";
        Car car2 = new Car(id, year, make, model, type, color, owner);

        Gson gson = new Gson();

        List<Car> carList = new LinkedList<>();
        carList.add(car2);

        when(carService.getCars("Blue", CarType.SEDAN.toString())).thenReturn(carList);

        mvc.perform(get("/api/cars?c=Blue&t=SEDAN"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(gson.toJson(carList)));
    }

    @Test
    public void addNewCar() throws Exception {
       when(carService.saveCar(any(Car.class))).thenReturn(testCar);

       String carJson = objectMapper.writeValueAsString(this.testCar);

       MockHttpServletRequestBuilder request = post("/api/cars")
                .contentType(MediaType.APPLICATION_JSON)
                .content(carJson);

       mvc.perform(request)
                .andExpect(status().isOk())
                .andExpect(content().json(carJson));
    }

    @Test
    public void getCarById() throws Exception {
        when(carService.getCarById(testCar.getId())).thenReturn(testCar);

        String carJson = objectMapper.writeValueAsString(this.testCar);

        mvc.perform(get("/api/cars/" + testCar.getId()))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(carJson));
    }

    @Test
    public void delete_car_exists() throws Exception {
        doNothing().when(carService).deleteCarById(testCar.getId());

        mvc.perform(delete("/api/cars/" + testCar.getId()))
                .andExpect(status().isOk());
    }

    @Test
    public void delete_car_does_not_exist() throws Exception {
        doThrow(RuntimeException.class).when(carService).deleteCarById(9999L);

        assertThrows(NestedServletException.class, () -> mvc.perform(delete("/api/cars/9999")));
    }

    @Test
    public void updateCarWithOwnersName() throws Exception{
        Gson gson = new Gson();

        when(carService.getCarById(1L)).thenReturn(testCar);
        testCar.setOwner("Fred");
        when(carService.saveCar(any())).thenReturn(testCar);

        Car alteredCar = new Car(testCar.getId(), testCar.getYear(), testCar.getMake(), testCar.getModel(), testCar.getType(), testCar.getColor(), "Fred");

        File jsonFile1 = new File("src/test/resources/test_patch_owner_user_obj.json");
        Scanner myReader1 = new Scanner(jsonFile1);
        StringBuilder sb1 = new StringBuilder();
        while (myReader1.hasNextLine()) {
            sb1.append(myReader1.nextLine());
        }
        myReader1.close();

        //patch request
        RequestBuilder patchRequest = MockMvcRequestBuilders.patch("/api/cars/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(sb1.toString());

        this.mvc.perform(patchRequest)
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(gson.toJson(alteredCar)));
    }

}
